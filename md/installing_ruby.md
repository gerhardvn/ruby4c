# Working online at first

To start this course without committing to much time you can start using an online hosted ruby environment. The following two site is a good starting point.

* [Try Ruby Playground](https://try.ruby-lang.org/playground/)
* [Solo Learn](https://www.sololearn.com)

A more advanced option would be [replit](https://replit.com/)

# Installation Instructions:

* Download Ruby Installer: Visit the [RubyInstaller website](https://rubyinstaller.org/) and download the latest version of the Ruby installer for Windows. Choose the appropriate version depending on your system architecture (32-bit or 64-bit).
* Run the Installer: Once the download is complete, double-click the downloaded file to start the installation process. You may need to grant administrative permissions to run the installer.
*Choose Installation Directory: Choose the installation directory where you want to install Ruby. The default location is C:\RubyXX, where XX is the version number. You can choose a different directory if you prefer.
* Select Additional Tasks: During the installation process, you can select additional tasks such as adding Ruby to the PATH environment variable, associating Ruby files with the Ruby interpreter, and installing the MSYS2 toolkit. It's recommended to select these options.
* Install Ruby: Click the "Install" button to start the installation process. The installer will download and install all necessary files and dependencies.
* Verify the Installation: Once the installation is complete, open a command prompt and type "ruby -v" to verify that Ruby is installed correctly. The command should return the installed version of Ruby.

Congratulations! You have successfully installed Ruby on Windows. 

# Run a ruby program

To write and test a "Hello, World!" program in Ruby, follow these steps:

* Open a text editor or an integrated development environment (IDE) to write your Ruby code.
* Create a new file with a .rb extension, such as hello_world.rb.
* In the file, write the following code: `puts "Hello, World!"` The puts method is used to print the string "Hello, World!" to the console.
* Save the file.

To test the program:
* Open a command prompt or terminal.
* Navigate to the directory where the hello_world.rb file is saved.
* Run the Ruby script by typing ruby hello_world.rb and pressing Enter.
* The output "Hello, World!" should be displayed in the console.

Congratulations! You have successfully written and tested a "Hello, World!" program in Ruby. You are ready to start.