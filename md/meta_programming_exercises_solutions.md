# Metaprogramming

# Reflection

## Exercises

### Create a program that reads a Ruby class name as input from the user and uses reflection to create an instance of that class.
``` lang-ruby
def create_instance(class_name)
  begin
    klass = Object.const_get(class_name)
    instance = klass.new
    puts "Instance of #{class_name} created successfully!"
    return instance
  rescue NameError
    puts "Error: Invalid class name '#{class_name}'"
  rescue StandardError => e
    puts "Error: #{e.message}"
  end
end

puts "Enter the name of the class:"
class_name = gets.chomp

instance = create_instance(class_name)

# Use the instance as needed
if instance
  instance.some_method
  instance.another_method
end
```

In this program, the create_instance method takes a class name as an argument and attempts to create an instance of that class using reflection. It uses Object.const_get to retrieve the class object based on the provided name and then calls new on that class object to create a new instance.

After creating the instance, the program demonstrates that the instance can be used by calling some hypothetical some_method and another_method on it. Feel free to replace these method calls with actual methods from the class you're working with.

Here's an example class that can be used to test the program:
``` lang-ruby
class MyClass
  def some_method
    puts "Inside some_method"
  end

  def another_method
    puts "Inside another_method"
  end
end
```

You can save this class definition in a separate file (e.g., my_class.rb) and require it in the main program where you create an instance of the class using reflection.

### Write a program that reads a method name and object instance as input from the user, and then uses reflection to invoke that method on the object.

### Create a program that reads a Ruby file as input, and then uses reflection to extract information about the classes and methods defined in that file.

### Write a program that reads a Ruby class name as input from the user, and then uses reflection to list all the public methods defined in that class.

### Create a program that uses reflection to determine if a given object instance is of a particular class or subclass.

# Dynamic method definition

## Exercises

### Write a program that takes user input for the name of a function and the code for that function, and dynamically defines the function using the input.

### Write a program that takes user input for the names of two functions and dynamically defines a third function that calls the other two functions in a specific order.

### Write a program that defines a class with a method that takes a block and dynamically defines a new method with the block as its implementation.

### Write a program that reads a JSON file containing a list of function names and their implementations, and dynamically defines each function in Ruby.

### Write a program that dynamically defines a set of functions based on a YAML file that describes their inputs and outputs. The program should be able to generate test cases for each function based on the input/output descriptions.

### Write a program that defines a set of functions for a calculator, and dynamically adds new functions based on user input for new operations.

### Write a program that defines a set of functions for working with arrays, and dynamically adds new functions based on user input for new operations, such as sorting or filtering.

# Open classes and modules

# DSLs (Domain Specific Languages)

# Code generation


