# Variables
Variables in C and Ruby share some similarities, such as their purpose of storing values, they also have several differences in their declaration, scope, memory management, type checking, and naming conventions.

## Declaration
In C, variables must be declared with their data type before they can be used. For example, to declare an integer variable in C, you would write `int x;`. In Ruby, variables are dynamically typed, which means that their data type is determined at runtime. To declare a variable in Ruby, you can simply assign a value to it, for example, `x = 10`.

Here is some C variable declarations followed by their Ruby equivalents.
```
int x;       // declaring an integer variable named x
double y;    // declaring a double precision floating point variable named y
char ch;     // declaring a character variable named ch
float f = 3.14;  // declaring and initializing a float variable named f
```
```
x = 10    # assigning an integer value 10 to a variable named x
y = 3.14  # assigning a float value 3.14 to a variable named y
ch = 'a'  # assigning a character value 'a' to a variable named ch
str = "hello"  # declaring and initializing a string variable named str
```
Because there is no type in Ruby the syntax for declaring variables in Ruby is more concise than in C.

In Ruby, you can also declare variables using symbols:
```
:name = "John"  # declaring and assigning a value to a symbol variable named :name
```

In Ruby, a symbol is a special type of object that represents a name or identifier, and is denoted by a colon (:) followed by the name. Symbol variables are variables that hold symbol objects.

Unlike strings, symbol objects are immutable, meaning that their value cannot be changed once they are created. This makes symbols more efficient for certain operations, such as comparing the equality of two identifiers, since a symbol only needs to be created once and can be reused throughout the program. WE will start using them when we get to the data structures chapter.

## Scope
In C, variables have block scope, which means that they are only accessible within the block of code where they are declared. In Ruby, variables have lexical scope, which means that they are accessible within the current scope and any nested scopes. Variables can have global, class, instance, local, or block scope.

Global scope in indicated by  a leading `$`.
```
$global_var = 10  # a global variable

def my_method
  local_var = 20  # a local variable

  puts $global_var  # outputs "10"
  puts local_var    # outputs "20"
end

local_var = 30 # a variable not visible in my_method
my_method
puts local_var    # outputs "30"
```

## Memory Management
In C, memory allocation and deallocation are done manually using functions like `malloc()` and `free()`, which can be a complex and error-prone task. In Ruby, memory management is automatic using a garbage collector, which means that you don't have to worry about memory allocation and deallocation.

Ruby's garbage collection uses a *mark and sweep* algorithm, which means that it automatically detects and frees objects that are no longer being used by the program. 

Ruby implements a "copy on write" optimization for object assignments. This means that if two variables are assigned the same object, Ruby will create a copy of the object only if one of the variables is modified. This can improve performance and reduce memory usage,

## Type Checking
In C which is statically typed, type checking is done at compile time, which means that if you try to assign a value of the wrong data type to a variable, the compiler will generate an error. Ruby have a dynamic type system and type checking is done at runtime, which means that you can assign values of different types to the same variable without generating an error.

Here's an example:
```
def multiply(a, b)
  if a.is_a?(Numeric) && b.is_a?(Numeric)
    return a * b
  else
    raise TypeError, "Arguments must be numeric"
  end
end

puts multiply(2, 3)      # Output: 6
puts multiply(2, "foo")  # Raises TypeError: Arguments must be numeric
```

In this example, the multiply method checks whether the a and b arguments are numeric types at runtime using the is_a? method. If the arguments are not numeric, the method raises a TypeError. This allows the programmer to catch type errors at runtime instead of having to rely on static type checking at compile-time.

## Naming Conventions
In C, variable names typically use snake_case or camelCase, and often start with a lowercase letter. In Ruby, variable names typically use snake_case, and often start with a lowercase letter or a lowercase symbol, such as `:my_variable`. Additionally, Ruby has special naming conventions for variables that are used as flags, such as `my_variable?` for boolean flags and `my_variable!` for destructive methods.

# Data Types

## Common types
### Integers

In Ruby, [`Fixnum`](https://ruby-doc.org/core-2.2.10/Fixnum.html) and [`Bignum`](https://ruby-doc.org/core-2.2.10/Bignum.html) are two built-in classes for representing integers of different sizes. The difference between them is in the range of values that they can represent.

`Fixnum` is used to represent integers that can fit within the native integer size of the machine running Ruby. This is the equivalent on the C `integer` type.

If an integer is too large to fit within the native integer size, Ruby automatically promotes it to a `Bignum`. `Bignum` is used to represent arbitrarily large integers, and can represent any integer that can fit within the available memory. `Bignum` rely on the [C GMP library](https://gmplib.org/).

Here's an example to illustrate the use of `Fixnum` and `Bignum` in Ruby:
```
x = 1234567890        # This is a Fixnum
puts x.class          # Output: Fixnum

y = x * x             # This is also a Fixnum
puts y.class          # Output: Fixnum

z = x ** 10           # This is too large for a Fixnum, so it is a Bignum
puts z.class          # Output: Bignum

w = z * z             # This is also a Bignum
puts w.class          # Output: Bignum
```

In the example, we create a variable x with the value 1234567890, which is a `Fixnum`. We then perform some operations on `x`, including multiplying it by itself to get `y` and raising it to the 10th power to get `z`. Because `z` is too large to fit in a `Fixnum`, Ruby automatically promotes it to a `Bignum`. Finally, we multiply `z` by itself to get `w`, which is also a `Bignum`.

### floating-point numbers
Ruby's [`Float`](https://ruby-doc.org/core-2.2.10/Float.html)  and C's `double` are both double-precision floating-point numbers, meaning they both use 64 bits to represent a floating-point value. However, there are some differences in how they handle certain values and operations.

One difference is that Ruby's `Float` supports `NaN` (Not a Number) and `Infinity` values, while C's `double` does not have native support for them. Instead, C uses special bit patterns to represent these values.

Another difference is that Ruby's `Float` uses a "rounded half-up" method for rounding, while C's `double` uses the "round to nearest even" method. This can result in slightly different rounding behaviour for certain values.

Overall, while there are some differences in the implementation details, Ruby's `Float` and C's `double` should behave similarly for most purposes.

### Strings
Ruby [String](https://ruby-doc.org/core-2.2.10/String.html)s and C strings are both used to represent sequences of characters, but they differ in several ways.

One major difference is that Ruby strings are mutable, while C strings are not. In other words, you can modify a Ruby string in-place, but if you want to change a C string, you need to allocate a new block of memory and copy the data over. Here's an example of how you can modify a Ruby string in-place using the `gsub!` method:
```
str = "hello world"
str.gsub!("world", "universe")
puts str # prints "hello universe"
```
In this example, the `gsub!` method is called on the string `str`, which replaces the substring "world" with "universe". The ! at the end of the method name indicates that the method modifies the string in-place, rather than returning a new string object. After the `gsub!` method is called, the contents of the `str` variable have been modified to "hello universe", demonstrating the mutability of Ruby strings.

Another difference is that Ruby strings are typically represented as objects in memory, with various built-in methods and properties, while C strings are usually just arrays of characters. This means that Ruby strings can be more expressive and flexible, but also may have more overhead in terms of memory usage and processing time. 

Ruby strings also support a wider range of encoding schemes than C strings, which are typically assumed to be ASCII or some other fixed-width character set. This can make it easier to work with non-ASCII characters and multilingual text in Ruby.

Finally, Ruby strings have various built-in methods for common string operations, such as substring extraction, search, and manipulation, which can make them more convenient to work with than C strings.

Here are some examples of the built-in capabilities of Ruby strings:

* Concatenation: Ruby strings can be easily concatenated using the `+` operator:
```
str1 = "hello"
str2 = "world"
puts str1 + " " + str2 # prints "hello world"
```
* Interpolation: Ruby strings can be interpolated with variables using the `#{}` syntax:
```
name = "Alice"
puts "Hello, #{name}!" # prints "Hello, Alice!"
```
* Substring extraction: Ruby strings can extract substrings using the `[]` operator:
```
str = "hello world"
puts str[0..4] # prints "hello"
```
* String formatting: Ruby strings can be formatted using the `sprintf` method:
```
name = "Bob"
age = 30
formatted_string = sprintf("My name is %s and I am %d years old.", name, age)
puts formatted_string # prints "My name is Bob and I am 30 years old."
```

These are just a few examples of the built-in capabilities of Ruby strings. Ruby also provides many other string-related methods and features, such as regular expression support, case conversion, and more.

### Arrays
(Array), as well as more complex types like hashes (Hash). We cover arrays in detail in the [data structures chapter](ruby_data_structures.md). 



## Special Data Types
Ruby also has special data types like symbols (Symbol) and regular expressions (Regexp), which are not present in C.

## Operators
Ruby has a number of methods and operators for manipulating data types, such as arithmetic operators for numbers, string concatenation (+) and comparison (==) operators for strings, and methods like split and join for manipulating arrays.

## Type Conversion
Ruby also supports type conversion, which allows variables to be converted between different data types. For example, you can convert a string to an integer using the `to_i` method or an integer to a string using the `to_s` method.

# Operators

# Control structures

# Functions