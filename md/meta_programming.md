# Metaprogramming
Metaprogramming is a powerful feature of Ruby that allows you to write code that writes code.

# Reflection

Ruby's reflection capabilities allow you to examine and modify the behaviour of Ruby objects at runtime. Reflection in programming refers to the ability of a program to examine and modify its own structure and behaviour at runtime. Reflection allows a program to introspect its own code, data structures, and objects, and make decisions based on this information.

In other words, reflection enables a program to "look at itself" and make decisions based on what it finds. This can be a powerful tool in many programming languages, including Ruby, where it is commonly used for metaprogramming and other advanced techniques. Some common examples of reflection in programming include examining and modifying object properties and methods at runtime, inspecting and manipulating the call stack, and dynamically generating and executing code.

programmers should learn about reflection methods such as `class`, `method`, and `instance_variables`.

Object#class: This method returns the class object of the current object. For example:
``` lang-ruby
str = "hello"
puts str.class # Output: String
```

Object#methods: This method returns an array of all the methods available on the current object. For example:
``` lang-ruby
str = "hello"
puts str.methods # Output: an array of all the available methods on a String object
```

Module#constants: This method returns an array of all the constants defined in a module. For example:
``` lang-ruby
module MyModule
  MY_CONSTANT = "hello"
end
```

puts MyModule.constants # Output: an array containing MY_CONSTANT
Kernel#eval: This method allows you to evaluate a string as if it were code. For example:
``` lang-ruby
str = "puts 'hello'"
eval(str) # Output: "hello"
```

Object#send: This method allows you to dynamically call a method on an object. For example:
``` lang-ruby
str = "hello"
method_name = "upcase"
puts str.send(method_name) # Output: "HELLO"
```
These are just a few examples of reflection in Ruby, there are many more ways to use it to dynamically manipulate and inspect objects at runtime.

## Exercises
Now a few exercises that can demonstrate the use of reflection in Ruby:

* Create a program that reads a Ruby class name as input from the user and uses reflection to create an instance of that class.
* Write a program that reads a method name and object instance as input from the user, and then uses reflection to invoke that method on the object.
* Create a program that reads a Ruby file as input, and then uses reflection to extract information about the classes and methods defined in that file.
* Write a program that reads a Ruby class name as input from the user, and then uses reflection to list all the public methods defined in that class.
* Create a program that uses reflection to determine if a given object instance is of a particular class or subclass.

[Exercises Solutions](meta_programming_exercises_solutions.html)

# Dynamic method definition

Ruby allows you to define methods dynamically at runtime, which can be useful in many situations. Here you will learn about methods such as `define_method`, `method_missing`, and `Module#define_method`.

Defining a function using define_method:
``` lang-ruby
class MyClass
  define_method :my_method do |arg|
    puts "You called my_method with #{arg}"
  end
end

obj = MyClass.new
obj.my_method("foo") #=> You called my_method with foo
```

Defining a function using define_singleton_method:
``` lang-ruby
obj = Object.new
obj.define_singleton_method :my_method do |arg|
  puts "You called my_method with #{arg}"
end

obj.my_method("bar") #=> You called my_method with bar
```

Defining a function using class_eval:
``` lang-ruby
class MyClass
  class_eval do
    define_method :my_method do |arg|
      puts "You called my_method with #{arg}"
    end
  end
end

obj = MyClass.new
obj.my_method("baz") #=> You called my_method with baz
```

These examples demonstrate how you can dynamically define methods at runtime in Ruby using metaprogramming techniques.

## Exercises
Here are some exercises to teach dynamic function definition in Ruby:

* Write a program that takes user input for the name of a function and the code for that function, and dynamically defines the function using the input.
* Write a program that takes user input for the names of two functions and dynamically defines a third function that calls the other two functions in a specific order.
* Write a program that defines a class with a method that takes a block and dynamically defines a new method with the block as its implementation.
* Write a program that reads a JSON file containing a list of function names and their implementations, and dynamically defines each function in Ruby.
* Write a program that dynamically defines a set of functions based on a YAML file that describes their inputs and outputs. The program should be able to generate test cases for each function based on the input/output descriptions.
* Write a program that defines a set of functions for a calculator, and dynamically adds new functions based on user input for new operations.
* Write a program that defines a set of functions for working with arrays, and dynamically adds new functions based on user input for new operations, such as sorting or filtering.

# Open classes and modules
In Ruby, you can open classes and modules at runtime and modify their behaviour. Experienced C programmers should learn about techniques such as class_eval, module_eval, and instance_eval to open and modify classes and modules. Programmers should learn about techniques such as `class_eval`, `module_eval`, and `instance_eval` to open and modify classes and modules.

# DSLs (Domain Specific Languages)
Ruby's metaprogramming capabilities allow you to define DSLs, which are small, specialized programming languages designed for specific tasks. You should learn  about techniques such as instance_exec and instance_eval to define and use DSLs.

# Code generation

Ruby's metaprogramming capabilities can be used to generate code dynamically at runtime. Programmers should learn about techniques such as eval and send to generate and execute code dynamically.

examples of using Ruby's metaprogramming capabilities to generate and execute code dynamically at runtime:

Using eval to dynamically execute code:
``` lang-ruby
x = 5
eval("puts x + 2")  # prints 7
```

Defining a new method at runtime using define_method:
``` lang-ruby
class MyClass
  define_method :my_method do |arg|
    puts "You passed in #{arg}"
  end
end

obj = MyClass.new
obj.my_method("hello")  # prints "You passed in hello"
```

Using send to dynamically call a method:
``` lang-ruby
class MyClass
  def my_method(arg)
    puts "You passed in #{arg}"
  end
end

obj = MyClass.new
obj.send(:my_method, "hello")  # prints "You passed in hello"
```

Using method_missing to dynamically handle method calls:
``` lang-ruby
class MyClass
  def method_missing(name, *args)
    if name == :my_method
      puts "You called my_method with arguments: #{args.inspect}"
    else
      super
    end
  end
end

obj = MyClass.new
obj.my_method("hello")  # prints "You called my_method with arguments: [\"hello\"]"
```

These are just a few examples of how Ruby's metaprogramming capabilities can be used to generate and execute code dynamically at runtime. There are many other techniques and use cases as well.

# Macros

Macros are a feature of some programming languages that allow you to define code that is executed at compile-time. Ruby does not have a direct equivalent to macros, but experienced C programmers should learn about how Ruby's metaprogramming capabilities can be used to achieve similar functionality.
In Ruby, macros are not used in the same way as in C, where they are preprocessor directives that modify code before compilation. However, Ruby does have a concept of metaprogramming, which allows for dynamic modification of code at runtime.

In the context of Ruby, macros typically refer to the use of metaprogramming techniques to define custom syntactic constructs or domain-specific languages (DSLs) that provide more concise and expressive ways of expressing certain operations or patterns.

One way to use metaprogramming in Ruby is to define custom syntactic constructs, which are new language constructs that behave like built-in language constructs but with customized functionality. This can be done by defining methods that modify the behavior of existing operators, or by defining new methods that can be called with custom syntax.

For example, you could use metaprogramming to define a custom syntax for defining and accessing configuration data in your application, like this:

``` lang-ruby
config :port => 8080, :host => 'localhost'
```

Here, config is a custom method that takes a hash of configuration options as its argument. The method uses metaprogramming techniques to define setter and getter methods for each option in the hash, so that you can access and modify the configuration data using a simple and intuitive syntax, like this:

``` lang-ruby
config.port  # returns 8080
config.port = 9090  # sets the port to 9090
```

This is just one example of how you can use metaprogramming in Ruby to define custom syntactic constructs that make your code more concise, expressive, and flexible.