# Data Structures
Ruby has several built-in data structures that are commonly used to store and manipulate data. Some of the important data structures in Ruby are:

Arrays: Arrays are ordered collections of elements that can be of any data type. Ruby arrays are dynamic, which means their size can be changed dynamically. They can be indexed using integers, and can be easily iterated over using various Ruby methods.

Hashes: Hashes are key-value pairs that allow fast lookups by key. In Ruby, hashes are implemented as a collection of key-value pairs, where each key is unique. Hashes can be used to store and retrieve data efficiently.

Strings: Strings are a sequence of characters that represent text. In Ruby, strings are represented as objects, and can be manipulated using various methods. They are often used to represent user input, filenames, and other text-based data.

# Arrays

# Hashes

# Strings

# Additional data structures
Ruby has additional data structures that you can use but is not cover by this course. Learning about them later.

Sets: Sets are a collection of unordered, unique elements. In Ruby, sets are implemented as a Hash where the key is the element in the set, and the value is always true. They can be used to efficiently store and manipulate unique elements. [Sets](https://ruby-doc.org/stdlib-2.7.1/libdoc/set/rdoc/Set.html)

Queues: Queues are a data structure that allows adding elements to the back and removing elements from the front. In Ruby, queues are implemented using the Queue class or can be created using the Array class. They are often used to process elements in a first-in, first-out (FIFO) manner.[Queues](https://ruby-doc.org/core-2.5.0/Queue.html)

Linked lists: Linked lists are a data structure that consists of a sequence of nodes, where each node contains a value and a reference to the next node. In Ruby, linked lists can be implemented using the LinkedList class in the [LinkedList gem](https://rubygems.org/gems/linked-list/). 